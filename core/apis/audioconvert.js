// Libraries/modules
var execSync = require('child_process').execSync;

var AudioConvert = {
	oggToWav: function(voiceFilePath) {
		var executablePath = "ffmpeg -i " + voiceFilePath + " -ac 1 -ar 22050 " + voiceFilePath.replace("ogg", "wav");
		var code = execSync(executablePath);
		console.log(code);
		return voiceFilePath.replace("ogg", "wav");
	},
	ogaToWav: function(voiceFilePath) {
		var executablePath = "ffmpeg -i " + voiceFilePath + " -ac 1 -ar 22050 " + voiceFilePath.replace("oga", "wav");
		var code = execSync(executablePath);
		console.log(code);
		return voiceFilePath.replace("ogg", "wav");
	},
	oggToFlac: function(voiceFilePath) {
		var newFilePath = this.oggToWav(voiceFilePath);
		return this.wavToFlac(newFilePath);
	},
	ogaToFlac: function(voiceFilePath) {
		var newFilePath = this.ogaToWav(voiceFilePath);
		return this.wavToFlac(newFilePath);
	},
	wavToFlac: function(voiceFilePath) {
		var executablePath = "sox " + voiceFilePath + " --channels=1 --bits=16 --rate=16000 --encoding=signed-integer --endian=little " + voiceFilePath.replace("wav", "raw");
		var code = execSync(executablePath);
		console.log(code);
		return voiceFilePath.replace("wav", "raw");
	},
}