eval(fs.readFileSync("./../core/apis/aimee.js")+'');
eval(fs.readFileSync("./../core/apis/gspeech.js")+'');
eval(fs.readFileSync("./../core/apis/ocr.js")+'');

var Core = {
	cognitions: null, 
	users: null,
	dump: function() {
	    var string1 = JSON.stringify(this.cognitions, null, 4, 'utf-8');
	    fs.writeFile(database + '/cognitions.json', string1);
		var string2 = JSON.stringify(this.users, null, 4, 'utf-8');
	    fs.writeFile(database + '/users.json', string2);
	},

	createAccount: function (login, pass) {
	    var id = this.cognitions.userCount++;

	    var user = {
	        id: id,
	        login: login,
	        pass: pass,
	        bases: {
	            count: 0,
	            list: []
	        }	
	    }

	    var string = JSON.stringify(user, null, 4, 'utf-8');
	    fs.writeFile(database + '/users/' + id +'.json', string);

	    this.users[login] = id;

	    Core.dump();

	    return user.id;
	},

	login: function(login, pass) {
		var id = this.users[login];
		if (id == undefined) {
			return -1;
		}
		var user = require(database + '/users/' + id + '.json');
		if (user['login'] == login && user['pass'] == pass) {
			return id;
		} else {
			return -1;
		}
	},

	getBasesList: function(user_id) {
		var user = require(database + '/users/' + user_id + '.json');
	    return user['bases']['list'];
	},

	createBase: function(id) {
	    var id = this.cognitions.knowbaseCount++;

	    var base = {
	        id: id,
	        owner: userId,
	        materials: {
	            count: 0,
	            list: []
	        }
	    }

	    var string = JSON.stringify(base, null, 4, 'utf-8');
	    fs.writeFile(database + '/bases/' + id +'.json', string);

	    var user = require(database + '/users/' + id + '.json');
	    user['bases']['count']++;
	    user['bases']['list'].push(base.id);

	    var string = JSON.stringify(user, null, 4, 'utf-8');
	    fs.writeFile(database + '/users/' + id +'.json', string);

	    Core.dump();
	},

	getTag: function(text) {
		var theme = Aimee.sendQuery('GET', 'answer', {
			key: text,
			count: 1,
			tag: ''
		});
		theme = JSON.parse(theme);
		var result = theme[0]['answer'];
		console.log(JSON.stringify(result, null, 4));
		if (result == -1 || result == undefined) {
			return -1;
		} else {
			return result.toLowerCase()
		}
	},

	addText: function(input_base, message_id, text, callback) {
		var material = {
	        id: ++(Core.cognitions.materialCount),
	        message: message_id,
	        tag: Core.getTag(text),
	        text: null,
	    };
		
	    var string = JSON.stringify(material, null, 4, 'utf-8');

	    fs.writeFile(database + '/materials/' + material.id + '.json', string);

	    // edit parent base
	    var base;
	    if (typeof(input_base) == 'number') {
	        base = require(database + '/bases/' + input_base + '.json');
	    } else {
	    	base = input_base;
	    }
	    base.materials.count += 1;
	    base.materials.list.push(material.id);

	    var string = JSON.stringify(base, null, 4, 'utf-8');
	    fs.writeFile(database + '/bases/' + base.id +'.json', string);

	    Core.dump();

	    callback(material.tag);
	},

	addPhoto: function(input_base, message_id, filepath, callback, text) {
		NodeCR.recognize(filepath, function(result) {
			var material = {
		        id: ++(Core.cognitions.materialCount),
		        message: message_id,
		        tag: Core.getTag(text + ' ' + result),
		        text: null,
		        file: {
		        	type: 'image',
		        	path: path.basename(filepath),
		        	text: result
		        }
		    };

		    if (text != undefined) {
		    	material.text = text;
		    }

		    var string = JSON.stringify(material, null, 4, 'utf-8');

		    fs.writeFile(database + '/materials/' + material.id + '.json', string);

		    // edit parent base
		    var base;
		    if (typeof(input_base) == 'number') {
		        base = require(database + '/bases/' + input_base + '.json');
		    } else {
		    	base = input_base;
		    }
		    base.materials.count += 1;
		    base.materials.list.push(material.id);

		    var string = JSON.stringify(base, null, 4, 'utf-8');
		    fs.writeFile(database + '/bases/' + base.id +'.json', string);

		    Core.dump();

		    callback(material.tag);
		});
	},

	addVoice: function(input_base, message_id, filepath, callback) {
		GSpeech.recognize(filepath, function(result) {
			var material = {
		        id: ++(Core.cognitions.materialCount),
		        message: message_id,
		        tag: Core.getTag(result),
		        text: null,
		        file: {
		        	type: 'voice',
		        	path: path.basename(filepath),
		        	text: result
		        }
		    };

		    var string = JSON.stringify(material, null, 4, 'utf-8');

		    fs.writeFile(database + '/materials/' + material.id + '.json', string);

		    // edit parent base
		    var base;
		    if (typeof(input_base) == 'number') {
		        base = require(database + '/bases/' + input_base + '.json');
		    } else {
		    	base = input_base;
		    }
		    base.materials.count += 1;
		    base.materials.list.push(material.id);

		    var string = JSON.stringify(base, null, 4, 'utf-8');
		    fs.writeFile(database + '/bases/' + base.id +'.json', string);

		    Core.dump();

		    callback(material.tag);
		});
	},

	getFromBase: function(user_id, input_base, text) {
		var base = require(database + '/bases/' + input_base + '.json');
		console.log(base);
		if (base.owner == user_id) {
			var messages = [];
			console.log(`Base size ${base.materials.count}`);
			for (var i = 0; i < base.materials.count; i++) {
				var material = require(database + '/materials/' + String(base.materials.list[i]) + '.json');
				console.log(`${material.tag} vs ${text}`);
				if (material.tag == text.toLowerCase()) {
					console.log('message pushed')
					messages.push(material.message);
				}
			}
			console.log(JSON.stringify(messages));
			return messages;
		}
		return [];
	},

	start: function () {
		this.cognitions = require(database + '/cognitions.json');
		this.users = require(database + '/users.json');
	}
};

// const express = require('express');
// const multer = require('multer')
// function startServer() {
// 	const server = express();

// 	var storage =  multer.diskStorage({
// 		destination: function (req, file, callback) {
// 			callback(null, path.resolve(__dirname + './../database'));
// 			console.log(file);
// 		},
// 		filename: function (req, file, callback) {
// 			callback(null, file.fieldname + '-' + Date.now());
// 			console.log(file);
// 		}
// 	});

// 	var upload = multer({ storage : storage }).single('img');

// 	server.post('/upload', function(req, res) {
// 		console.log(req);
// 		upload(req, res, function(err) {
// 	        if (err) {
// 	        	console.log('error');
// 	            return res.end("Error uploading file.");
// 	        }
// 	        res.end("File is uploaded");
// 	    });
// 	});

// 	server.get('/', function(req, res) {
// 		res.sendFile(path.resolve(__dirname + '/../web/index.html'));
// 	});

// 	server.get('/createAccount', function(req, res) {
// 		var login = req.query.login;
// 		var pass = req.query.pass;
// 		createAccount(login, pass);
// 		res.send('success');
// 	})

// 	server.get('/getFromBase', function(req, res) {
// 		var query = req.query.text;
// 		Aimee.sendQuery();
// 	});

// 	server.listen(9000, function() {
// 		console.log('express started')
// 	});
// }