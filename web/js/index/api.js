var API = {
	// Get API methods
	getAllCardsData: function() {
		// ajax
		/*$.ajax({
			type: "POST",
			url: "/getAllCardsData",
			data: "",
			success: function(data){
				DataLoader.cardsData = JSON.parse(data);
				DataLoader.loadAllCards();
			}
		});*/
		DataLoader.cardsData = [
			{
				"audio": "1.mp3",
				"image": "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_162c12eaa2e%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_162c12eaa2e%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22106.3984375%22%20y%3D%2296.3%22%3E286x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E",
				"title": "Моя первая аудиозапись и картинка",
				"text": "Текст первой записи полностью.",
				"confidence": 0.9
			},
			{
				"audio": "",
				"image": "",
				"title": "2. Ничего",
				"text": "Текст 2-й записи полностью.",
				"confidence": 0.8
			},
			{
				"audio": "1.mp3",
				"image": "",
				"title": "Только аудио(3)",
				"text": "Текст 3-й записи полностью.",
				"confidence": 0.74
			},
			{
				"audio": "",
				"image": "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_162c12eaa2e%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_162c12eaa2e%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22106.3984375%22%20y%3D%2296.3%22%3E286x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E",
				"title": "Только картиночка(4)",
				"text": "Текст 4-й записи полностью.",
				"confidence": 0.66
			},
		];
		DataLoader.loadAllCards();
	},
	// Send API methods
	sendNewVoiceNote: function() {
		var blobURL = (window.URL || window.webkitURL).createObjectURL(DataLoader.recordedVoiceDataBlob);
		console.log(blobURL);
		var filename = "voicenote.wav";
		var sendingData = new FormData();
		sendingData.append("voicenote", DataLoader.recordedVoiceDataBlob);
		$.ajax({
			url: "/sendNewVoiceNote",
			type: "POST",
			data: sendingData,
			contentType: false,
			processData: false,
			success: function(data) {
				alert(data);
			},
			error: function() {
				alert("error!");
			}
		});
	},
	sendNewImageNote: function() {
		var imageNoteFile = $("#NewImageNoteFile");
		var sendingData = new FormData;
		sendingData.append("imagenote", imageNoteFile.prop('files')[0]);
		$.ajax({
			url: "/sendNewImageNote",
			data: sendingData,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (data) {
				alert(data);
			},
			error: function() {
				alert("error!");
			}
		});
	},
	sendNewTextNote: function() {
		$.ajax({
			type: "POST",
			url: "/sendNewTextNote",
			data: "textnote=" + $("#NewTextNoteText").val(),
			success: function(data) {
				data = JSON.parse(data);
				if(data["status"] == "ok") {
					alert("Текстовая заметка добавлена!");
					UI.selfUpdate();
				} else {
					alert("Возникла ошибка при добавлении текстовой заметки!");
				}
			}
		});
	},
}