var UI = {
	hideAllBlocks: function() {
		$("#MainBlock").hide();
		$("#TakeNoteBlock").hide();
		$("#CardsBlock").hide();
	},
	switchBlock: function(blockName) {
		this.hideAllBlocks();
		switch(blockName) {
			case "Main": {
				$("#MainBlock").show("slow");
				break;
			};
			case "TakeNote": {
				$("#TakeNoteBlock").show("slow");
				break;
			};
			case "Cards": {
				$("#CardsBlock").show("slow");
				break;
			};
			default: {
				break;
			};
		}
	},
	hideAllElems: function() {
		$("#TakeNoteMenuElem").hide();
		$("#TakeNewVoiceNoteElem").hide();
		$("#TakeNewImageNoteElem").hide();
		$("#TakeNewTextNoteElem").hide();
	},
	showElem: function(elemName) {
		this.hideAllElems();
		switch(elemName) {
			case "TakeNoteMenu": {
				$("#TakeNoteMenuElem").show("slow");
				break;
			};
			case "TakeNewVoiceNote": {
				$("#TakeNewVoiceNoteElem").show("slow");
				break;
			};
			case "TakeNewImageNote": {
				$("#TakeNewImageNoteElem").show("slow");
				break;
			};
			case "TakeNewTextNote": {
				$("#TakeNewTextNoteElem").show("slow");
				break;
			};
			default: {
				break;
			};
		}
	},
	selfUpdate: function() {
		window.location.href = window.location.href;
	},
	initAudioRecorder: function() {
		//
	},
	initAll: function() {
		API.getAllCardsData();
	}
}

// Voice recorder