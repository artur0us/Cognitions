// Libraries/modules
const FS = require("fs");
// Local libraries
eval(FS.readFileSync("core/apis/aimee.js")+'');
eval(FS.readFileSync("core/apis/ocr.js")+'');
eval(FS.readFileSync("core/apis/gspeech.js")+'');
// Database files(e.g.: settings)
var AppSettings = require("./database/settings.json");

var App = {
	init: function() {
		this.run();
	},
	run: function() {
		if(AppSettings.common.debug)
			this.testAnything();
		else {
			console.log("[i] Starting *Cognitions*...");
		}
	},
	testAnything: function() {
		console.log(Aimee.sendQuery("GET", "ai_config"));
		var data = {
			min_distinct_operators: 5,
			min_answers: 3,
			hello_phrase: "What are you doing?",
			lsa_max_add_time: 2,
			lsa_max_confidence: 0.65
		}
		console.log(Aimee.sendQuery("POST", "ai_config", data));
		OCR.doOCR("/home/artur0us/dev/cognitions/data/images/1.jpg");
		GSpeech.recognize("/home/artur0us/dev/cognitions/data/audio/1.ogg");
		console.log("All testing completed!!!");
	}
}

App.init();